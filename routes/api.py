from tornado.web import RequestHandler
from tornado.template import Loader
from util import DB, create_id

class ShorterHandler(RequestHandler):

	loader = Loader("views")

	def initialize(self, fast_mode):
		self.fast = fast_mode

	def get(self, uid):
		url = DB.get(uid)

		if url is None:
			self.send_error(404, msg='ID not found.')
		else:
			if self.fast:
				self.redirect(url)
			else:
				template = self.loader.load('go.html').generate(title='Go!!', url=url)

				self.write(template)

class UrlHandlerHandler(RequestHandler):
	def prepare(self):
		if self.request.headers.get('Content-Type', '').split(';')[0] == 'application/json':
			from json import loads as json_decode
			self.args = json_decode(self.request.body)
		elif self.request.headers.get('Content-Type', '').split(';')[0] == 'multipart/form-data':
			self.args = {}
			for index in self.request.arguments:
				self.args[index] = self.request.arguments[index][0].decode()
		else:
			self.args = {}

	def post(self):
		if 'url' not in self.args:
			self.send_error(400, msg='URL is a required field')
		else:
			data = {
				'url': self.args['url'],
				'id': create_id()
			}

			DB.set(data['id'], data['url'])

			self.write(data)

	def delete(self):
		if 'id' not in self.args:
			self.send_error(400, msg='ID is a required field')
		else:
			self.add_header('Content-Type', 'application/json')
			DB.delete(self.args['id'])
			self.write('"deleted"')