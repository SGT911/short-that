from tornado.web import RequestHandler
from tornado.template import Loader

class ViewHandler(RequestHandler):

	loader = Loader("views")

	def get(self):
		template = self.loader.load('create.html').generate(title='Short URL')

		self.write(template)