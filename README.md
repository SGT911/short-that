# ShortThat
*A small app to short your URL*

## Maded with

+ Python - **Tornado**
+ Redis



## Environment Variables

+ `PORT` Set the where  ShortThat will listen (By default `8888`)
+ `DB_HOST` Host of Redis (By Default `localhost`)
+ `DB_PORT` Port of Redis (By default `6379`)
+ `DB_DBNUM` Number for ShortThat DB (By default `0`)



## Route map

+ `/` The form to Short the URL
+ `/url/(id)` Page to redirect
+ `/url/fast/(id)` Direct redirection
+ `/api` The API for ShorThat



## Api methods

+ `POST` To create the URL
  + Accept Multipart Form & JSON
  + Require the field:
    + `url` Is string URL like
  + Return two fields in JSON:
    + `url` The url where redirect the app
    + `ID` the id to redirect with `/url` or `/url/fast`
+ `DELETE`
  + Accept Multipart Form & JSON
  + Require the field:
    + `ID`
  + Return a `"deleted"`