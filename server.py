#!/bin/env python
# @autor SGT911

from tornado.web import Application, RequestHandler
from tornado.ioloop import IOLoop

def make_app() -> Application:
	from routes.api import ShorterHandler, UrlHandlerHandler
	from routes.home import ViewHandler

	route_map = [
		(r'/', ViewHandler),
		(r'/url/(.*)', ShorterHandler, dict(fast_mode=False)),
		(r'/url/fast/(.*)', ShorterHandler, dict(fast_mode=True)),
		(r'/api', UrlHandlerHandler)
	]

	return Application(route_map)

if __name__ == '__main__':
	from os import environ
	import sys

	# AsyncIO BUG for Windows
	if sys.platform == 'win32':
		import asyncio
		asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

	# Create app & Start listening
	app = make_app()
	app.listen(environ.get('PORT', 8888))
	IOLoop.current().start()