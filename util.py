from random import choice
from redis import Redis
from os import environ

# Redis DB Connection
DB_CONF = {
	'host': environ.get('DB_HOST', 'localhost'),
	'port': int(environ.get('DB_PORT', 6379)),
	'db': int(environ.get('DB_DBNUM', 0)),
}

DB = Redis(host=DB_CONF['host'], port=DB_CONF['port'], db=DB_CONF['db'])

# ID generator
CHARS='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_'

def create_id() -> str:
	res = ''
	for i in range(10):
		tmp = choice(CHARS)
		res = f'{res}{tmp}'

	return res